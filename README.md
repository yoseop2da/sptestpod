# TestHSPod

[![CI Status](http://img.shields.io/travis/hojin@hsociety.co.kr/TestHSPod.svg?style=flat)](https://travis-ci.org/hojin@hsociety.co.kr/TestHSPod)
[![Version](https://img.shields.io/cocoapods/v/TestHSPod.svg?style=flat)](http://cocoapods.org/pods/TestHSPod)
[![License](https://img.shields.io/cocoapods/l/TestHSPod.svg?style=flat)](http://cocoapods.org/pods/TestHSPod)
[![Platform](https://img.shields.io/cocoapods/p/TestHSPod.svg?style=flat)](http://cocoapods.org/pods/TestHSPod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TestHSPod is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'TestHSPod'
```

## Author

hojin@hsociety.co.kr, hojin@hsociety.co.kr

## License

TestHSPod is available under the MIT license. See the LICENSE file for more info.
